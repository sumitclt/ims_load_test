var http = require('http');

console.log("Starting Test...")

const maxCount = 5;

const retryFunc = (cc, req, res) => {
    if(res != null && res.searchResponse != null && res.searchResponse.availCachedTimeMap != null && Object.keys(res.searchResponse.availCachedTimeMap).length != 0 && cc <= maxCount) {
        setTimeout(() => sendRequest(cc + 1, req, retryFunc),1000);
    } else if(res.searchResponse != null && res.searchResponse.availCachedTimeMap != null && Object.keys(res.searchResponse.availCachedTimeMap).length != 0) {
        console.log("Expired Request ", req, ", Expired", res.searchResponse.availCachedTimeMap);
    } else if(res.searchResponse != null) {
        console.log("Completed Request ", req, ", Sol Count", getSearchResponseSolutionCount(res.searchResponse));
    } else {
        console.log(res);
    }
};

const sectors = ['AIP','AJL','AMD','ATQ','BBI','BDQ','BHO','BLR','BOM','BUP','CCJ','CCU','CDP','CJB','CNN','COK','DED','DEL','DHM','DIB','DMU','DXB','GAU','GAY','GOI','GOP','GWL','HBX','HJR','HYD','IDR','IMF','ISK','IXA','IXB','IXC','IXD','IXE','IXG','IXJ','IXL','IXM','IXR','IXS','IXU','IXY','IXZ','JAI','JDH','JLR','JRG','JRH','JSA','KLH','KNU','KUU','LKO','MAA','MYQ','NAG','NDC','PAT','PBD','PNQ','PNY','RAJ','RDP','RJA','RPR','SAG','SHL','SLV','STV','SXR','SXV','TCR','TIR','TRV','TRZ','UDR','VGA','VNS','VTZ'];
const suppliers = [{
    name: 'INDIGO',
    credKey: 'production_IN_search_indigo_newskies_new',
    carriers: ['INDIGO']
},
{
     name: 'SPICEJET',
     credKey: 'production_in_search_spicejet_newskies_new',
     carriers: ['SPICEJET']
 },
 {
    name: 'RADIXX',
    credKey: 'production_IN_search_goair_primary_navi',
    carriers: ['GO']
},
{
    name: 'GALILEO',
    credKey: 'production_IN_search_galileo_lfs_domestic_5LK4_edge',
    carriers: ['AI']
},
{
        name: 'AMADEUS',
    credKey: 'production_IN_search_amadeus_dom_india',
    carriers: ['UK']
}
];


setInterval(() => generateAndSendRequest(sectors),200);


function generateAndSendRequest(sectors) {
    const sector = getRandomSector(sectors);
    const date = getRandomDate(180);
    const ddate = format(date.departDate);
    const rdate = format(date.returnDate);
    const supplier = getRandomSupplier();
    const postData = createRequest(sector.from, sector.to, ddate,getRandomInt(4) + 1,getRandomInt(2),0,supplier,rdate);
    sendRequest(1, postData,retryFunc);
}

function getRandomSupplier() {
    const index = (getRandomInt(123458)*31)%suppliers.length;
    return suppliers[index];
}

function getRandomDate(untilMaxDays) {
    const daysToAdd = (getRandomInt(123458998)*31)%untilMaxDays;
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() + daysToAdd);
    var returnDate = null;
    if(daysToAdd%5 == 0) {
        returnDate = new Date();
        returnDate.setDate(returnDate.getDate() + daysToAdd + (getRandomInt(123458998)*31)%(untilMaxDays-daysToAdd));
    }
    return {departDate: currentDate, returnDate: returnDate};
}

function format(date) {
    if(null == date) {
        return null;
    }
    return date.getFullYear() + "-" + appendLeadingZeroes(date.getMonth() + 1) + "-" + appendLeadingZeroes(date.getDate());
}

function appendLeadingZeroes(n){
    if(n <= 9){
      return "0" + n;
    }
    return n
  }

function getRandomSector(sectors) {
    return getRandomSector(sectors,0,2);
}

function getRandomSector(sectors,currentCount,maxCount) {
    const fromIndex = (getRandomInt(1000000)*31)%sectors.length;
    const toIndex = (getRandomInt(1000000)*31)%sectors.length;
    if(fromIndex != toIndex) {
        return {from:sectors[fromIndex],to:sectors[toIndex]};
    } else if(currentCount <= maxCount){
        return getRandomSector(sectors, currentCount + 1, maxCount);
    } else {
        return {from:sectors[0],to:sectors[1]};
    }
}


function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}


function getSolutionCount(solArr) {
    if(solArr != null) {
        return solArr.length;
    }
    return 0;
}

function getSearchResponseSolutionCount(searchResponse) {
    return 'ow=' + getSolutionCount(searchResponse.onwardSolutions) + ',r=' + getSolutionCount(searchResponse.returnSolutions)
            + ',rt=' + getSolutionCount(searchResponse.roundtripSolutions) + ', zero_cache=' + searchResponse.zeroResultsCaching
}

function sendRequest(callCount, requestBody, callback) {
    var start = new Date().getTime();
    var options = {
        host: "localhost",
        port: "6080",
        path: "/flights/availability",
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Connection": "Keep-Alive"
        }
    };
    
    var req = http.request(options, function (res) {
        var responseString = "";
    
        res.on("data", function (data) {
            responseString += data;
            
        });
        res.on("end", function () {
            var timeTaken = new Date().getTime() - start;
            console.log("Total time taken ", timeTaken);
            if(res.statusCode == 200) {
                callback(callCount, requestBody, JSON.parse(responseString));
            } else {
                console.log(responseString);
            }
        });
    });
    
    req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`);
      });
        
    req.write(requestBody);
    req.end()
}


function createRequest(from, to, departDate, adt=1, chd=0, inf=0, supplier, returnDate) {
    return JSON.stringify({
        sc: {
            sellingCurrency: 'INR',
            sellingCountry: 'IN',
            itineraryId: null,
            fareCategory: "retail",
            psid: "bc-v2-ebbd7105-a732-4d88-90cf-bea8adccf370",
            domestic: true,
            from: from,
            to: to,
            departDate: departDate,
            returnDate: returnDate,
            adults: adt,
            children: chd,
            infants: inf,
            cabinType: "ECONOMY",
            preferredCarriers: supplier.carriers,
            preferredAirlines: [
                "OP",
                "2T",
                "CTS",
                "M1",
                "EK",
                "9W-K",
                "AI",
                "I5",
                "6E",
                "IX",
                "CTA",
                "G8",
                "G9",
                "SG",
                "EY",
                "LB",
                "UK",
                "V1",
                "9W",
                "S2",
                "DC"
            ],
            international: false
        },
        so: {
            connectorDataSource: "CACHE_ONLY"
        },
        ck: supplier.credKey,
        supp: supplier.name
    })
}



